import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/javacode',
    name: 'JavaCode',
    component: () => import(/* webpackChunkName: "about" */ '../views/JavaCode.vue')
  },
  {
    path: '/sqlcode',
    name: 'SqlCode',
    component: () => import(/* webpackChunkName: "about" */ '../views/SqlCode.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
