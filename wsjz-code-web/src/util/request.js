import axios from 'axios'

const baseURL = 'http://localhost:9998'

axios.interceptors.response.use(response=>{
    return response.data
})

export function postAction(url, parameter) {
    return axios({
        url: baseURL + url,
        method: 'post',
        data: parameter,
        headers: { 'Content-Type': 'application/json' }
    })
}

export function getAction(url, parameter) {
    return axios({
        url: baseURL + url,
        method: 'get',
        params: parameter,
    })
} 