# wsjz-code

#### 介绍
在线编程，目前支持 java语言和 mysql增删改查语句

#### 软件架构
软件架构说明
前端使用 vue3+element-plus,首页特效使用 tsparticles，在线代码编辑器使用 codemirror  
后端使用 springboot，jdbc，动态内存编译 java，定时任务，线程池，阻塞队列等技术  


#### 安装教程
语言：java8+  
依赖管理：maven  
数据库：MySQL5.7+  

#### 使用说明
1.建表sql在sql文件夹下  
2.后台项目安装maven后导入编辑器，直接启动springboot项目  
3.前端首次使用需要 npm install,然后执行 npm run serve 启动项目  


#### 页面效果
首页  
![输入图片说明](%E6%95%88%E6%9E%9C%E5%9B%BE/1.png)

首页动态图  
![输入图片说明](%E6%95%88%E6%9E%9C%E5%9B%BE/2.gif)

java在线编程  
![输入图片说明](%E6%95%88%E6%9E%9C%E5%9B%BE/3.gif)

识别出java源代码编译错误  
![输入图片说明](%E6%95%88%E6%9E%9C%E5%9B%BE/4.gif)

识别出java运行错误  
![输入图片说明](%E6%95%88%E6%9E%9C%E5%9B%BE/5.gif)

sql在线编程  
![输入图片说明](%E6%95%88%E6%9E%9C%E5%9B%BE/6.gif)
![输入图片说明](%E6%95%88%E6%9E%9C%E5%9B%BE/7.gif)

修改编辑器主题背景色  
![输入图片说明](%E6%95%88%E6%9E%9C%E5%9B%BE/8.gif)
