package com.wsjzzcbq.wsjzcode.queue;

import com.wsjzzcbq.wsjzcode.bean.Code;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * CodeExecuteQueue
 *
 * @author wsjz
 * @date 2023/07/06
 */
public class CodeExecuteQueue {

    public static ArrayBlockingQueue<Code> queue = new ArrayBlockingQueue<>(1000);
}
