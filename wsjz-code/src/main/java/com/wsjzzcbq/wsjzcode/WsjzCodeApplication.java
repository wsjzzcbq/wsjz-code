package com.wsjzzcbq.wsjzcode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WsjzCodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(WsjzCodeApplication.class, args);
    }

}
