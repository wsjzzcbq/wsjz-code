package com.wsjzzcbq.wsjzcode.bean;

import com.wsjzzcbq.wsjzcode.consts.LangTypeEnum;
import lombok.Data;

/**
 * Code
 *
 * @author wsjz
 * @date 2023/07/06
 */
@Data
public class Code {

    /**
     * 每次运行的唯一标识
     */
    private String number;

    /**
     * 代码类型
     */
    private LangTypeEnum type;

    private String code;
}
