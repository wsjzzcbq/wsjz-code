package com.wsjzzcbq.wsjzcode.bean;

import lombok.Data;

/**
 * R
 *
 * @author wsjz
 * @date 2023/07/06
 */
@Data
public class R<T> {

    private static final String SUCCESS = "成功";
    private static final String ERROR = "失败";

    /**
     * 1成功 0失败
     */
    private Integer code;

    private String msg;

    private T data;

    public static<T> R<T> success() {
        return success(SUCCESS, null);
    }

    public static<T> R<T> success(String msg) {
        return success(msg, null);
    }

    public static<T> R<T> success(T data) {
        return success(SUCCESS, data);
    }

    public static<T> R<T> success(String msg, T data) {
        return custom(1, msg, data);
    }

    public static<T> R<T> error() {
        return error(ERROR);
    }

    public static<T> R<T> error(String msg) {
        return error(msg, null);
    }

    public static<T> R<T> error(T data) {
        return error(ERROR, data);
    }

    public static<T> R<T> error(String msg, T data) {
        return custom(0, msg, data);
    }

    public static<T> R<T> custom(Integer code, String msg) {
        R<T> r = new R<>();
        r.setCode(code);
        r.setMsg(msg);
        return r;
    }

    public static<T> R<T> custom(Integer code, String msg, T data) {
        R<T> r = new R<>();
        r.setCode(code);
        r.setMsg(msg);
        r.setData(data);
        return r;
    }
}
