package com.wsjzzcbq.wsjzcode.bean;

import lombok.Data;

/**
 * ExecuteResult
 *
 * @author wsjz
 * @date 2023/07/06
 */
@Data
public class ExecuteResult {

    public static final int SUCCESS = 1;
    public static final int ERROR = 0;

    /**
     * 执行结果 1 程序执行成功 0 程序执行出错
     */
    private Integer result;

    private String msg;

    private Object data;

    private Code code;
}
