package com.wsjzzcbq.wsjzcode.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * JavaCode
 *
 * @author wsjz
 * @date 2023/07/06
 */
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Data
public class JavaCode extends Code{

    /**
     * 程序执行的方法名称
     */
    private String method;

}
