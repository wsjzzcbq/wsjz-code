package com.wsjzzcbq.wsjzcode.bean;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * SqlCode
 *
 * @author wsjz
 * @date 2023/07/07
 */
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Data
public class SqlCode extends Code{
}
