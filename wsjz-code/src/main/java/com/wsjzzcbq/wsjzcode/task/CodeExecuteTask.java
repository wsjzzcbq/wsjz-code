package com.wsjzzcbq.wsjzcode.task;

import com.wsjzzcbq.wsjzcode.bean.Code;
import com.wsjzzcbq.wsjzcode.bean.JavaCode;
import com.wsjzzcbq.wsjzcode.bean.SqlCode;
import com.wsjzzcbq.wsjzcode.execute.Executer;
import com.wsjzzcbq.wsjzcode.queue.CodeExecuteQueue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.util.Map;

/**
 * CodeExecuteTask
 *
 * @author wsjz
 * @date 2023/07/06
 */
@Slf4j
@EnableScheduling
@Component
public class CodeExecuteTask {

    @Autowired
    private Map<String, Executer> executers;

    @Scheduled(fixedRate = 1000)
    public void codeExecute() {
        try {
            log.info("开始");
            Code code = CodeExecuteQueue.queue.take();
            if (code instanceof JavaCode) {
                executers.get("javaExecuter").exec(code);
            }
            if (code instanceof SqlCode) {
                executers.get("sqlExecuter").exec(code);
            }
        } catch (InterruptedException e) {
            log.error(e.getMessage(), e);
        }
        log.info("codeExecute");
    }
}
