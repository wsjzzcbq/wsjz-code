package com.wsjzzcbq.wsjzcode.compile;

import com.wsjzzcbq.wsjzcode.consts.SqlTypeEnum;
import org.springframework.stereotype.Component;

/**
 * SqlCompiler
 *
 * @author wsjz
 * @date 2023/07/07
 */
@Component
public class SqlCompiler {

    public SqlTypeEnum sqlType(String sql) {
        String content = sql.trim().toUpperCase();
        if (content.indexOf(SqlTypeEnum.SELECT.getValue()) == 0) {
            return SqlTypeEnum.SELECT;
        }
        if (content.indexOf(SqlTypeEnum.INSERT.getValue()) == 0) {
            return SqlTypeEnum.INSERT;
        }
        if (content.indexOf(SqlTypeEnum.UPDATE.getValue()) == 0) {
            return SqlTypeEnum.UPDATE;
        }
        if (content.indexOf(SqlTypeEnum.DELETE.getValue()) == 0) {
            return SqlTypeEnum.DELETE;
        }
        return null;
    }

}
