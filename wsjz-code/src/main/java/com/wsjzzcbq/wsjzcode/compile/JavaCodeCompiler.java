package com.wsjzzcbq.wsjzcode.compile;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

/**
 * JavaCodeCompiler
 *
 * @author wsjz
 * @date 2023/07/12
 */
public interface JavaCodeCompiler {

    /**
     * 编译java源代码
     * 返回map {result:true} 编译成功 {result:false,error:"errorMsg"} 编译失败，error是编译错误信息
     * @param code
     * @return
     */
    Map<String, Object> compile(String code);

    /**
     * 运行java 类className的methodName方法
     * @param className
     * @param methodName
     * @return
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws InstantiationException
     */
    Object run(String className, String methodName) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException;
}
