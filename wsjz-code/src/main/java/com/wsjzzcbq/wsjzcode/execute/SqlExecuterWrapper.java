package com.wsjzzcbq.wsjzcode.execute;

import com.alibaba.fastjson2.JSONObject;
import com.wsjzzcbq.wsjzcode.bean.ExecuteResult;
import com.wsjzzcbq.wsjzcode.code.CodeRunResultMap;
import com.wsjzzcbq.wsjzcode.consts.SqlTypeEnum;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * SqlExecuterWrapper
 *
 * @author wsjz
 * @date 2023/07/08
 */
public abstract class SqlExecuterWrapper implements Executer{

    protected void execSqlUpdate(PreparedStatement ps, SqlTypeEnum sqlType, String number, ExecuteResult result) throws SQLException {
        int count = ps.executeUpdate();
        JSONObject json = new JSONObject();
        json.put("sqlType", sqlType.getValue());
        json.put("count", count);
        result.setResult(ExecuteResult.SUCCESS);
        result.setData(json);
        CodeRunResultMap.map.put(number, result);
    }
}
