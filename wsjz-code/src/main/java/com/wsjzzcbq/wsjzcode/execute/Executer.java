package com.wsjzzcbq.wsjzcode.execute;

import com.wsjzzcbq.wsjzcode.bean.Code;

/**
 * Executer 执行器
 *
 * @author wsjz
 * @date 2023/07/07
 */
public interface Executer {

    void exec(Code code);
}
