package com.wsjzzcbq.wsjzcode.code;

import com.wsjzzcbq.wsjzcode.bean.ExecuteResult;
import java.util.concurrent.ConcurrentHashMap;

/**
 * CodeRunResultMap
 *
 * @author wsjz
 * @date 2023/07/06
 */
public class CodeRunResultMap {

    public static ConcurrentHashMap<String, ExecuteResult> map = new ConcurrentHashMap<>();
}
