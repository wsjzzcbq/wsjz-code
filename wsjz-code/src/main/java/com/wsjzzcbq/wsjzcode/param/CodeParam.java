package com.wsjzzcbq.wsjzcode.param;

import lombok.Data;

/**
 * CodeParam
 *
 * @author wsjz
 * @date 2023/07/07
 */
@Data
public class CodeParam {

    private String code;
}
