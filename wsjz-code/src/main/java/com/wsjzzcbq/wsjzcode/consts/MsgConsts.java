package com.wsjzzcbq.wsjzcode.consts;

/**
 * MsgConsts
 *
 * @author wsjz
 * @date 2023/07/09
 */
public class MsgConsts {

    public static final String COMPILE_ERROR = "编译错误";

    public static final String RUNTIME_EXCEPTION = "运行异常";

    public static final String CODE_NOT_EMPTY = "代码不能为空";

    public static final String JAVA_CLASSNAME_NOT_EMPTY = "Java类名不能为空";

    public static final String JAVA_CLASSNAME_NOT_COMPLY_FIRST_LETTER_UPPERCASE = "Java类名不符合首字母大写规范";

    public static final String SQL_NOT_EMPTY = "sql不能为空";
}
