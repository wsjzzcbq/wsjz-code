package com.wsjzzcbq.wsjzcode.consts;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * LangTypeEnum
 *
 * @author wsjz
 * @date 2023/07/06
 */
@AllArgsConstructor
public enum LangTypeEnum {

    /**
     * java语言
     */
    Java(".java"),
    Sql("sql");

    @Getter
    private String value;
}
