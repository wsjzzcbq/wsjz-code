package com.wsjzzcbq.wsjzcode.consts;

import java.text.MessageFormat;

/**
 * MessageConsts
 *
 * @author wsjz
 * @date 2023/07/07
 */
public class MessageConsts {

    private static final String queueMessage = "您前面有{0}个人在排队，请等待";

    public static String getQueueMessage(int number) {
        return MessageFormat.format(queueMessage, number);
    }
}
