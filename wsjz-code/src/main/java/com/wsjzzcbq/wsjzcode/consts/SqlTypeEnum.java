package com.wsjzzcbq.wsjzcode.consts;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * SqlTypeEnum
 *
 * @author wsjz
 * @date 2023/07/07
 */
@AllArgsConstructor
public enum SqlTypeEnum {

    /**
     * 查询
     */
    SELECT("SELECT"),
    /**
     * 添加
     */
    INSERT("INSERT"),
    /**
     * 修改
     */
    UPDATE("UPDATE"),
    /**
     * 删除
     */
    DELETE("DELETE");

    @Getter
    private String value;
}
