package com.wsjzzcbq.wsjzcode.service.impl;

import com.alibaba.fastjson2.JSONObject;
import com.wsjzzcbq.wsjzcode.bean.ExecuteResult;
import com.wsjzzcbq.wsjzcode.bean.JavaCode;
import com.wsjzzcbq.wsjzcode.bean.R;
import com.wsjzzcbq.wsjzcode.code.CodeRunResultMap;
import com.wsjzzcbq.wsjzcode.consts.LangTypeEnum;
import com.wsjzzcbq.wsjzcode.consts.MsgConsts;
import com.wsjzzcbq.wsjzcode.param.CodeParam;
import com.wsjzzcbq.wsjzcode.queue.CodeExecuteQueue;
import com.wsjzzcbq.wsjzcode.service.JavaCodeService;
import com.wsjzzcbq.wsjzcode.util.JavaCodeUitls;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import java.util.UUID;

/**
 * JavaCodeServiceImpl
 *
 * @author wsjz
 * @date 2023/07/06
 */
@Service
public class JavaCodeServiceImpl implements JavaCodeService {

    @Override
    public R<?> execute(CodeParam param) {
        if (StringUtils.isBlank(param.getCode())) {
            return R.custom(2, MsgConsts.CODE_NOT_EMPTY);
        }
        if (JavaCodeUitls.checkClassNameisNull(param.getCode())) {
            return R.custom(2, MsgConsts.JAVA_CLASSNAME_NOT_EMPTY);
        }
        if (!JavaCodeUitls.checkClassNameisSpecification(param.getCode())) {
            return R.custom(2, MsgConsts.JAVA_CLASSNAME_NOT_COMPLY_FIRST_LETTER_UPPERCASE);
        }

        JavaCode javaCode = new JavaCode();
        String number = UUID.randomUUID().toString();
        javaCode.setNumber(number);
        javaCode.setCode(param.getCode());
        javaCode.setType(LangTypeEnum.Java);
        javaCode.setMethod("sort");
        try {
            CodeExecuteQueue.queue.put(javaCode);
            CodeRunResultMap.map.put(number, new ExecuteResult());
        } catch (InterruptedException e) {
            return R.custom(2, e.getMessage());
        }

        JSONObject json = queuingInstruction(number);

        return R.success(json);
    }

}
