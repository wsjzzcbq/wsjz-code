package com.wsjzzcbq.wsjzcode.service;

import com.alibaba.fastjson2.JSONObject;
import com.wsjzzcbq.wsjzcode.bean.R;
import com.wsjzzcbq.wsjzcode.consts.MessageConsts;
import com.wsjzzcbq.wsjzcode.param.CodeParam;
import com.wsjzzcbq.wsjzcode.queue.CodeExecuteQueue;

/**
 * CodeService
 *
 * @author wsjz
 * @date 2023/07/07
 */
public interface CodeService {

    R<?> execute(CodeParam param);

    default JSONObject queuingInstruction(String number) {
        JSONObject json = new JSONObject(3);
        json.put("number", number);
        //队列中数据超过1说明有其他人在排队
        if (CodeExecuteQueue.queue.size() > 1) {
            json.put("code", 0);
            json.put("msg", MessageConsts.getQueueMessage(CodeExecuteQueue.queue.size()));
        } else {
            json.put("code", 1);
        }
        return json;
    }
}
