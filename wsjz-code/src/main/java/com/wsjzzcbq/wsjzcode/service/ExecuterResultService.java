package com.wsjzzcbq.wsjzcode.service;

import com.wsjzzcbq.wsjzcode.bean.R;

/**
 * ExecuterResultService
 *
 * @author wsjz
 * @date 2023/07/06
 */
public interface ExecuterResultService {

    R<?> get(String number);
}
