package com.wsjzzcbq.wsjzcode.service.impl;

import com.wsjzzcbq.wsjzcode.bean.ExecuteResult;
import com.wsjzzcbq.wsjzcode.bean.R;
import com.wsjzzcbq.wsjzcode.code.CodeRunResultMap;
import com.wsjzzcbq.wsjzcode.service.ExecuterResultService;
import org.springframework.stereotype.Service;

/**
 * ExecuterResultServiceImpl
 *
 * @author wsjz
 * @date 2023/07/06
 */
@Service
public class ExecuterResultServiceImpl implements ExecuterResultService {
    @Override
    public R<?> get(String number) {
        ExecuteResult executeResult = CodeRunResultMap.map.get(number);
        if (executeResult.getResult() == null) {
            return R.custom(2, "");
        }
        executeResult.setCode(null);
        CodeRunResultMap.map.remove(number, executeResult);
        return R.success(executeResult);
    }
}
