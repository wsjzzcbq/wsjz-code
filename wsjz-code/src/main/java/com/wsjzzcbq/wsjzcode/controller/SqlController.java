package com.wsjzzcbq.wsjzcode.controller;

import com.wsjzzcbq.wsjzcode.bean.R;
import com.wsjzzcbq.wsjzcode.param.SqlCodeParam;
import com.wsjzzcbq.wsjzcode.service.SqlService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * SqlController
 *
 * @author wsjz
 * @date 2023/07/07
 */
@Slf4j
@CrossOrigin
@RequestMapping("/code/sql")
@RestController
public class SqlController {

    @Autowired
    private SqlService sqlService;

    @RequestMapping("/execute")
    public R<?> execute(@RequestBody SqlCodeParam param) {
        try {
            return sqlService.execute(param);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return R.error();
        }
    }
}
