package com.wsjzzcbq.wsjzcode.controller;

import com.wsjzzcbq.wsjzcode.bean.R;
import com.wsjzzcbq.wsjzcode.service.ExecuterResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * ExecuterResultController
 *
 * @author wsjz
 * @date 2023/07/06
 */
@CrossOrigin
@RequestMapping("/execute/result")
@RestController
public class ExecuterResultController {

    @Autowired
    private ExecuterResultService executerResultService;

    @GetMapping("/get/{number}")
    public R<?> get(@PathVariable("number") String number) {
        try {
            return executerResultService.get(number);
        } catch (Exception e) {
            return R.error(e);
        }
    }
}
