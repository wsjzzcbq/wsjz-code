package com.wsjzzcbq.wsjzcode.controller;

import com.wsjzzcbq.wsjzcode.bean.R;
import com.wsjzzcbq.wsjzcode.param.JavaCodeParam;
import com.wsjzzcbq.wsjzcode.service.JavaCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * JavaCodeController
 *
 * @author wsjz
 * @date 2023/07/06
 */
@CrossOrigin
@RequestMapping("/code/java")
@RestController
public class JavaCodeController {

    @Autowired
    private JavaCodeService javaCodeService;

    @PostMapping("/execute")
    public R<?> execute(@RequestBody JavaCodeParam param) {
        try {
            return javaCodeService.execute(param);
        } catch (Exception e) {
            return R.error();
        }
    }
}
