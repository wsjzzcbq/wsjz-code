package com.wsjzzcbq.wsjzcode.util;

import org.apache.commons.lang3.StringUtils;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * JavaCodeUitls
 *
 * @author wsjz
 * @date 2023/07/06
 */
public class JavaCodeUitls {

    private static final Pattern pattern = Pattern.compile("(?<=\\n|\\A)(?:public\\s)?(class|interface|enum)\\s([^\\n\\s]*)");

    /**
     * 通过正则获取java类名
     * @param code
     * @return
     */
    public static String getClassName(String code) {
        Matcher matcher = pattern.matcher(code);
        if (matcher.find()) {
            return matcher.group(2);
        }
        return null;
    }

    /**
     * 非空判断
     * @param code
     * @return
     */
    public static boolean checkClassNameisNull(String code) {
        String className = getClassName(code);
        if (StringUtils.isEmpty(className)) {
            return true;
        }
        return false;
    }

    /**
     * 首字母大写
     * @param code
     * @return
     */
    public static boolean checkClassNameisSpecification(String code) {
        String className = getClassName(code);
        char firstLetter = className.charAt(0);
        if (Character.isUpperCase(firstLetter)) {
            return true;
        }
        return false;
    }
}
