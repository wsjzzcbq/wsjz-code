package com.wsjzzcbq.wsjzcode.util;

import java.io.File;

/**
 * FileUtils
 *
 * @author wsjz
 * @date 2023/07/06
 */
public class FileUtils {

    public static void deleteDirectory(String directory) {
        deleteDirectory(new File(directory));
    }

    public static void deleteDirectory(File directory) {
        for (File file : directory.listFiles()) {
            if (file.isDirectory()) {
                deleteDirectory(file);
                file.delete();
            }
            if (file.isFile()) {
                file.delete();
            }
        }
    }
}
