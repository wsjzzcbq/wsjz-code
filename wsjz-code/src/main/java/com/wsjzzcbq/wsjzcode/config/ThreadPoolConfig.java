package com.wsjzzcbq.wsjzcode.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/**
 * ThreadPoolConfig
 *
 * @author wsjz
 * @date 2023/07/09
 */
@Configuration
public class ThreadPoolConfig {

    /**
     * 配置多线程执行定时任务
     * @return
     */
    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        taskScheduler.setPoolSize(3);
        return taskScheduler;
    }
}
